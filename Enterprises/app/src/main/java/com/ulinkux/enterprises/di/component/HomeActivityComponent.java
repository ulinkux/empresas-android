package com.ulinkux.enterprises.di.component;


import com.ulinkux.enterprises.HomeActivity;
import com.ulinkux.enterprises.di.module.AdapterModule;
import com.ulinkux.enterprises.di.scopes.ApplicationScope;

import dagger.Component;

@ApplicationScope
@Component(modules = AdapterModule.class, dependencies = ApplicationComponent.class)
public interface HomeActivityComponent {

    void injectHomeActivity(HomeActivity homeActivity);
}
