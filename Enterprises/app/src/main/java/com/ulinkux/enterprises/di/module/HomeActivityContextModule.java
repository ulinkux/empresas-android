package com.ulinkux.enterprises.di.module;

import android.content.Context;

import com.ulinkux.enterprises.HomeActivity;
import com.ulinkux.enterprises.di.qualifier.ActivityContext;
import com.ulinkux.enterprises.di.scopes.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeActivityContextModule {
    private HomeActivity homeActivity;
    private Context context;

    public HomeActivityContextModule(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
        context = homeActivity;
    }

    @Provides
    @ActivityScope
    public HomeActivity provideHomeActivity() {
        return homeActivity;
    }

    @Provides
    @ActivityScope
    @ActivityContext
    public Context provideContext() {
        return context;
    }

}
