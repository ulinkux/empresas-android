package com.ulinkux.enterprises.di.module;

import androidx.recyclerview.widget.RecyclerView;

import com.ulinkux.enterprises.HomeActivity;
import com.ulinkux.enterprises.di.scopes.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module(includes = {HomeActivityContextModule.class})
public class AdapterModule {

//    @Provides
//    @ActivityScope
//    public RecyclerViewAdapter getEnterpriseList(RecyclerViewAdapter.ClickListener clickListener) {
//        return new RecyclerViewAdapter(clickListener);
//    }
//
//    @Provides
//    @ActivityScope
//    public RecyclerViewAdapter.ClickListener getClickListener(HomeActivity homeActivity) {
//        return mainActivity;
//    }
}
