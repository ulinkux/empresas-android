package com.ulinkux.enterprises.mvp;

import com.ulinkux.enterprises.retrofit.APIInterface;

import javax.inject.Inject;

public class MainActivityPresenterImpl implements MainActivityContract.Presenter {

    APIInterface apiInterface;
    MainActivityContract.View mView;

    @Inject
    public MainActivityPresenterImpl(APIInterface apiInterface, MainActivityContract.View mView) {
        this.apiInterface = apiInterface;
        this.mView = mView;
    }

    @Override
    public void handleClickLogin() {

    }

    @Override
    public void onDestroy() {

    }
}
