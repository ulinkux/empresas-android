package com.ulinkux.enterprises.pojo;

import com.google.gson.annotations.SerializedName;
import com.ulinkux.enterprises.pojo.EnterpriseType;

public class Enterprise {

    @SerializedName("id")
    public String id;
    @SerializedName("enterprise_name")
    public String enterprise_name;
    @SerializedName("description")
    public String description;
    @SerializedName("email_enterprise")
    private String email_enterprise;
    @SerializedName("facebook")
    private String facebook;
    @SerializedName("twitter")
    private String twitter;
    @SerializedName("linkedin")
    private String linkedin;
    @SerializedName("phone")
    private String phone;
    @SerializedName("own_enterprise")
    private boolean own_enterprise;
    @SerializedName("photo")
    private String photo;
    @SerializedName("value")
    private float value;
    @SerializedName("shares")
    private float shares;
    @SerializedName("share_price")
    private float share_price;
    @SerializedName("own_shares")
    private float own_shares;
    @SerializedName("city")
    private String city;
    @SerializedName("country")
    private String country;
    @SerializedName("enterprise_type")
    private EnterpriseType enterpriseType;




}
