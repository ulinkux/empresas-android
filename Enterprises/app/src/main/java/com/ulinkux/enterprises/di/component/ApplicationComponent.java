package com.ulinkux.enterprises.di.component;


import android.content.Context;

import com.ulinkux.enterprises.MyApplication;
import com.ulinkux.enterprises.di.module.ContextModule;
import com.ulinkux.enterprises.di.module.RetrofitModule;
import com.ulinkux.enterprises.di.qualifier.ApplicationContext;
import com.ulinkux.enterprises.di.scopes.ApplicationScope;
import com.ulinkux.enterprises.retrofit.APIInterface;

import dagger.Component;

@ApplicationScope
@Component(modules = {ContextModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    APIInterface getApiInterface();

    @ApplicationContext
    Context getContext();

    void injectApplication(MyApplication myApplication);
}
