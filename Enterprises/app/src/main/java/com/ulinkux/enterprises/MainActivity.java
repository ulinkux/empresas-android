package com.ulinkux.enterprises;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.ulinkux.enterprises.di.component.ApplicationComponent;
import com.ulinkux.enterprises.di.component.MainActivityComponent;
import com.ulinkux.enterprises.di.module.MainActivityContextModule;
import com.ulinkux.enterprises.di.qualifier.ActivityContext;
import com.ulinkux.enterprises.di.qualifier.ApplicationContext;
import com.ulinkux.enterprises.mvp.MainActivityPresenterImpl;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    MainActivityComponent mainActivityComponent;

    @Inject
    @ApplicationContext
    public Context mContext;

    @Inject
    @ActivityContext
    public Context activityContext;

    @Inject
    MainActivityPresenterImpl presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    ApplicationComponent applicationComponent = MyApplication.get(this).getApplicationComponent();
    mainActivityComponent = DaggerMainActivityComponent.builder()
        .mainActivityContextModule(new MainActivityContextModule(this))
        .applicationComponent(applicationComponent)
        .build();

    mainActivityComponent.injectMainActivity(this);

}
