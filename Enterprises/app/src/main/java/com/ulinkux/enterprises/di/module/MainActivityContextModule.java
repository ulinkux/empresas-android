package com.ulinkux.enterprises.di.module;

import android.content.Context;

import com.ulinkux.enterprises.MainActivity;
import com.ulinkux.enterprises.di.qualifier.ActivityContext;
import com.ulinkux.enterprises.di.scopes.ActivityScope;
import com.ulinkux.enterprises.di.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityContextModule {
    private MainActivity mainActivity;

    public Context context;

    public MainActivityContextModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        context = mainActivity;
    }

    @Provides
    @ActivityScope
    public MainActivity providesMainActivity() {
        return mainActivity;
    }

    @Provides
    @ActivityScope
    @ActivityContext
    public Context provideContext() {
        return context;
    }
}
