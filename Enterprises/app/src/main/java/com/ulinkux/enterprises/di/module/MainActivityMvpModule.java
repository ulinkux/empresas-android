package com.ulinkux.enterprises.di.module;

import com.ulinkux.enterprises.di.scopes.ActivityScope;
import com.ulinkux.enterprises.mvp.MainActivityContract;

import dagger.Provides;

public class MainActivityMvpModule {
    private final MainActivityContract.View mView;

    public MainActivityMvpModule(MainActivityContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @ActivityScope
    MainActivityContract.View provideView(){
        return this.mView;
    }
}
