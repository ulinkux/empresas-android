package com.ulinkux.enterprises.retrofit;

import com.ulinkux.enterprises.pojo.Enterprise;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import rx.Observable;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;



public interface APIInterface {

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @POST("users/auth/sign_in")
    Observable<Response<ResponseBody>> login(@Body Login login);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("enterprises")
    Observable<EnterpriseResponse> getEnterpriseData(@Query("name") String name, @HeaderMap Map<String, String> headers);


}
