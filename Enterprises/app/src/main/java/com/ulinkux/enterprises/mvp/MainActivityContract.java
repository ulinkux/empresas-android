package com.ulinkux.enterprises.mvp;

public interface MainActivityContract {
    interface  View {
        void handleLogin();
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        void handleClickLogin();

        void onDestroy();
    }
}
