package com.ulinkux.enterprises.di.component;

import android.content.Context;

import com.ulinkux.enterprises.MainActivity;
import com.ulinkux.enterprises.di.module.AdapterModule;
import com.ulinkux.enterprises.di.qualifier.ActivityContext;
import com.ulinkux.enterprises.di.scopes.ApplicationScope;

import dagger.Component;

@ApplicationScope
@Component(modules = AdapterModule.class, dependencies = ApplicationComponent.class)
public interface MainActivityComponent {

    @ActivityContext
    Context getContext();
    void injectMainActivity(MainActivity mainActivity);
}
