package com.ulinkux.enterprises.di.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface ActivityContext {
}
