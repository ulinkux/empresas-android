package com.ulinkux.enterprises.di.module;

import android.content.Context;

import com.ulinkux.enterprises.DetailsActivity;
import com.ulinkux.enterprises.di.qualifier.ActivityContext;
import com.ulinkux.enterprises.di.scopes.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailsActivityContextModule {
    private Context context;
    private DetailsActivity detailsActivity;

    public DetailsActivityContextModule(DetailsActivity detailsActivity) {
        this.detailsActivity = detailsActivity;
        context = detailsActivity;
    }

    @Provides
    @ActivityScope
    public DetailsActivity provideDetailsActivity() {
        return detailsActivity;
    }

    @Provides
    @ActivityScope
    @ActivityContext
    public Context provideContext() {
        return context;
    }
}
