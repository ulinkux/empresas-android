package com.ulinkux.enterprises.pojo;

import com.google.gson.annotations.SerializedName;

public class EnterpriseType {
    @SerializedName("id")
    public int id;
    @SerializedName("enterprise_type_name")
    public String enterprise_type_name;
}
